import callWebApi from "src/helpers/webApiHelper";

export const addComment = async request => {
    const response = await callWebApi({
        endpoint: "/api/comments",
        type: "POST",
        request
    });
    return response.json();
};

export const getComment = async id => {
    const response = await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: "GET"
    });
    return response.json();
};

export const deleteComment = async id => {
    const response = await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: "DELETE"
    });
    return response.json();
};

export const updateComment = async (id, body) => {
    const response = await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: "PUT",
        request: {
            body
        }
    });
    return response.json();
};

export const restoreComment = async id => {
    const response = await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: "PUT",
        request: {
            restore: true
        }
    });
    return response.json();
};
