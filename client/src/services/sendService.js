import callWebApi from "src/helpers/webApiHelper";

export const sendEmail = async (link, email) => {
    await callWebApi({
        endpoint: `/api/send`,
        type: "POST",
        request: {
            link,
            email
        }
    });
};
