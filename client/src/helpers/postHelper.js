export const counterDiff = (like, isLike, createdAt, updatedAt) => {
    let likeDiff = 0;
    let dislikeDiff = 0;
    switch (isLike) {
        case true:
            likeDiff = 1;
            if (createdAt !== updatedAt) {
                dislikeDiff = -1;
            }
            break;
        case false:
            dislikeDiff = 1;
            if (createdAt !== updatedAt) {
                likeDiff = -1;
            }
            break;
        default:
            if (like) {
                likeDiff = -1;
            } else {
                dislikeDiff = -1;
            }
    }
    return {
        likeDiff,
        dislikeDiff
    };
};
