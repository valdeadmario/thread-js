import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import * as imageService from "src/services/imageService";
import ExpandedPost from "src/containers/ExpandedPost";
import Post from "src/components/Post";
import AddPost from "src/components/AddPost";
import SharedPostLink from "src/components/SharedPostLink";
import { Radio, Loader, Form } from "semantic-ui-react";
import InfiniteScroll from "react-infinite-scroller";
import {
    loadPosts,
    loadMorePosts,
    likePost,
    dislikePost,
    toggleExpandedPost,
    addPost,
    deletePost,
    updatePost,
    restorePost,
    sendLink
} from "./actions";

import styles from "./styles.module.scss";

class Thread extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sharedPostId: undefined,
            showOwnPosts: false,
            showNotOwnPosts: false,
            showLikedPosts: false,
            showDeletedPosts: false
        };
        this.postsFilter = {
            userId: undefined,
            from: 0,
            count: 10,
            not: false
        };
    }

    tooglePosts = keyword => {
        this.setState(
            state => ({
                showOwnPosts: false,
                showNotOwnPosts: false,
                showLikedPosts: false,
                showDeletedPosts: false,
                [keyword]: !state[keyword]
            }),
            () => {
                Object.assign(this.postsFilter, {
                    userId: this.state[keyword]
                        ? `${this.props.userId}`
                        : undefined,
                    from: 0,
                    not: this.state.showNotOwnPosts,
                    liked: this.state.showLikedPosts,
                    deleted: this.state.showDeletedPosts
                });
                this.props.loadPosts(this.postsFilter);
                this.postsFilter.from = this.postsFilter.count; // for next scroll
            }
        );
    };

    loadMorePosts = () => {
        this.props.loadMorePosts(this.postsFilter);
        const { from, count } = this.postsFilter;
        this.postsFilter.from = from + count;
    };

    sharePost = sharedPostId => {
        this.setState({ sharedPostId });
    };

    closeSharePost = () => {
        this.setState({ sharedPostId: undefined });
    };

    uploadImage = file => imageService.uploadImage(file);

    render() {
        const { expandedPost, hasMorePosts, ...props } = this.props;
        let { posts = [] } = this.props;
        const {
            showOwnPosts,
            showNotOwnPosts,
            showLikedPosts,
            sharedPostId,
            showDeletedPosts
        } = this.state;

        return (
            <div className={styles.threadContent}>
                <div className={styles.addPostForm}>
                    <AddPost
                        addPost={props.addPost}
                        uploadImage={this.uploadImage}
                    />
                </div>
                <div className={styles.toolbar}>
                    <Form>
                        <Radio
                            toggle
                            label="Show only my posts"
                            checked={showOwnPosts}
                            onChange={() => this.tooglePosts("showOwnPosts")}
                        />
                        <Radio
                            toggle
                            label="Show all posts without my"
                            checked={showNotOwnPosts}
                            onChange={() => this.tooglePosts("showNotOwnPosts")}
                        />
                        <Radio
                            toggle
                            label="Show all which i liked"
                            checked={showLikedPosts}
                            onChange={() => this.tooglePosts("showLikedPosts")}
                        />
                        <Radio
                            toggle
                            label="Show deleted posts"
                            checked={showDeletedPosts}
                            onChange={() =>
                                this.tooglePosts("showDeletedPosts")
                            }
                        />
                    </Form>
                </div>
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadMorePosts}
                    hasMore={hasMorePosts}
                    loader={<Loader active inline="centered" key={0} />}
                >
                    {posts.map(post => (
                        <Post
                            userId={this.props.userId}
                            post={post}
                            likePost={props.likePost}
                            dislikePost={props.dislikePost}
                            toggleExpandedPost={props.toggleExpandedPost}
                            sharePost={this.sharePost}
                            updatePost={props.updatePost}
                            deletePost={props.deletePost}
                            restorePost={props.restorePost}
                            key={post.id}
                        />
                    ))}
                </InfiniteScroll>
                {expandedPost && (
                    <ExpandedPost
                        sharePost={this.sharePost}
                        currentUserId={this.props.userId}
                    />
                )}
                {sharedPostId && (
                    <SharedPostLink
                        postId={sharedPostId}
                        close={this.closeSharePost}
                        sendLink={props.sendLink}
                    />
                )}
            </div>
        );
    }
}

Thread.propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object),
    hasMorePosts: PropTypes.bool,
    expandedPost: PropTypes.objectOf(PropTypes.any),
    sharedPostId: PropTypes.string,
    userId: PropTypes.string,
    loadPosts: PropTypes.func.isRequired,
    loadMorePosts: PropTypes.func.isRequired,
    likePost: PropTypes.func.isRequired,
    dislikePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    addPost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired,
    updatePost: PropTypes.func.isRequired,
    restorePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
    posts: [],
    hasMorePosts: true,
    expandedPost: undefined,
    sharedPostId: undefined,
    userId: undefined
};

const mapStateToProps = rootState => ({
    posts: rootState.posts.posts,
    hasMorePosts: rootState.posts.hasMorePosts,
    expandedPost: rootState.posts.expandedPost,
    userId: rootState.profile.user.id
});

const actions = {
    loadPosts,
    loadMorePosts,
    likePost,
    dislikePost,
    toggleExpandedPost,
    addPost,
    deletePost,
    updatePost,
    restorePost,
    sendLink
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Thread);
