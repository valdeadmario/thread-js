import * as postService from "src/services/postService";
import * as commentService from "src/services/commentService";
import * as sendService from "src/services/sendService";
import { counterDiff } from "src/helpers/postHelper";
import {
    ADD_POST,
    LOAD_MORE_POSTS,
    SET_ALL_POSTS,
    SET_EXPANDED_POST
} from "./actionTypes";

const setPostsAction = posts => ({
    type: SET_ALL_POSTS,
    posts
});

const addMorePostsAction = posts => ({
    type: LOAD_MORE_POSTS,
    posts
});

const addPostAction = post => ({
    type: ADD_POST,
    post
});

const setExpandedPostAction = post => ({
    type: SET_EXPANDED_POST,
    post
});

export const loadPosts = filter => async dispatch => {
    const posts = await postService.getAllPosts(filter);
    dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
    const {
        posts: { posts }
    } = getRootState();
    const loadedPosts = await postService.getAllPosts(filter);
    const filteredPosts = loadedPosts.filter(
        post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
    );
    dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
    const post = await postService.getPost(postId);
    dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
    const { id } = await postService.addPost(post);
    const newPost = await postService.getPost(id);
    dispatch(addPostAction(newPost));
};

export const restorePost = id => async (dispatch, getRootState) => {
    await postService.restorePost(id);
    const {
        posts: { posts }
    } = getRootState();
    const updated = posts.map(post => {
        if (post.id === id) {
            return { ...post, deletedAt: null };
        }
        return post;
    });
    dispatch(setPostsAction(updated));
};

export const toggleExpandedPost = postId => async dispatch => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
    const { isLike, createdAt, updatedAt } = await postService.likePost(postId);

    const { likeDiff, dislikeDiff } = counterDiff(
        true,
        isLike,
        createdAt,
        updatedAt
    );

    const mapLikes = post => ({
        ...post,
        likeCount: Number(post.likeCount) + likeDiff,
        dislikeCount: Number(post.dislikeCount) + dislikeDiff
    });

    const {
        posts: { posts, expandedPost }
    } = getRootState();
    const updated = posts.map(post =>
        post.id !== postId ? post : mapLikes(post)
    );

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapLikes(expandedPost)));
    }
};

export const updatePost = (id, data) => async (dispatch, getRootState) => {
    await postService.updatePost(id, data);
    //const posts = postService.getAllPosts();
    const {
        posts: { posts }
    } = getRootState();
    const updated = posts.map(post => {
        let item;
        if (post.id === id) {
            item = {
                ...post,
                body: data
            };
        } else {
            item = post;
        }
        return item;
    });

    dispatch(setPostsAction(updated));
};

export const deletePost = postId => async (dispatch, getRootState) => {
    await postService.deletePost(postId);
    const {
        posts: { posts }
    } = getRootState();
    const idx = posts.findIndex(post => post.id === postId);
    const newPosts = [...posts.slice(0, idx), ...posts.slice(idx + 1)];
    dispatch(setPostsAction(newPosts));
};

export const dislikePost = postId => async (dispatch, getRootState) => {
    const { isLike, createdAt, updatedAt } = await postService.dislikePost(
        postId
    );
    const { likeDiff, dislikeDiff } = counterDiff(
        false,
        isLike,
        createdAt,
        updatedAt
    );

    const mapDislikes = post => ({
        ...post,
        likeCount: Number(post.likeCount) + likeDiff,
        dislikeCount: Number(post.dislikeCount) + dislikeDiff
    });

    const {
        posts: { posts, expandedPost }
    } = getRootState();
    const updated = posts.map(post =>
        post.id !== postId ? post : mapDislikes(post)
    );

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
    }
};

export const addComment = request => async (dispatch, getRootState) => {
    const { id } = await commentService.addComment(request);
    const comment = await commentService.getComment(id);

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) + 1,
        comments: [...(post.comments || []), comment] // comment is taken from the current closure
    });

    const {
        posts: { posts, expandedPost }
    } = getRootState();
    const updated = posts.map(post =>
        post.id !== comment.postId ? post : mapComments(post)
    );

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const restoreComment = (id, postId) => async (
    dispatch,
    getRootState
) => {
    await commentService.restoreComment(id);

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) + 1,
        comments:
            (post.comments &&
                post.comments.map(comment =>
                    comment.id === id
                        ? { ...comment, deletedAt: null }
                        : comment
                )) ||
            []
    });

    const {
        posts: { posts, expandedPost }
    } = getRootState();
    const updated = posts.map(post =>
        post.id !== postId ? post : mapComments(post)
    );
    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const updateComment = (commentId, data, postId) => async (
    dispatch,
    getRootState
) => {
    await commentService.updateComment(commentId, data);
    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount),
        comments:
            (post.comments &&
                post.comments.map(comment =>
                    comment.id === commentId
                        ? { ...comment, body: data }
                        : comment
                )) ||
            []
    });

    const {
        posts: { posts, expandedPost }
    } = getRootState();
    const updated = posts.map(post =>
        post.id !== postId ? post : mapComments(post)
    );

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const deleteComment = (postId, commentId) => async (
    dispatch,
    getRootState
) => {
    await commentService.deleteComment(commentId);

    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) - 1,
        comments:
            (post.comments &&
                post.comments.filter(comment => comment.id !== commentId)) ||
            []
    });

    const {
        posts: { posts, expandedPost }
    } = getRootState();

    const updated = posts.map(post =>
        post.id !== postId ? post : mapComments(post)
    );

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const sendLink = (link, email) => async dispatch => {
    await sendService.sendEmail(link, email);
};
