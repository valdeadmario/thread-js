import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import validator from "validator";
import { connect } from "react-redux";
import { getUserImgLink } from "src/helpers/imageHelper";
import { Grid, Image, Input, Button, Form } from "semantic-ui-react";

const initialState = {
    email: "",
    username: "",
    isLoading: false,
    isEmailValid: true,
    isUsernameValid: true
};

const Profile = ({ user }) => {
    const [state, setState] = useState(initialState);

    useEffect(() => {
        console.log(user);
        const { username, email } = user;
        setState({ ...state, username, email });
    }, []);

    const validateEmail = () => {
        const { email } = state;
        const isEmailValid = validator.isEmail(email);
        setState({
            ...state,
            isEmailValid
        });
        return isEmailValid;
    };

    const validateUsername = () => {
        const { username } = state;
        const isUsernameValid = !validator.isEmpty(username);
        setState({
            ...state,
            isUsernameValid
        });
        return isUsernameValid;
    };

    const emailChanged = email =>
        setState({ ...state, email, isEmailValid: true });

    const usernameChanged = username =>
        setState({ ...state, username, isUsernameValid: true });

    const validateForm = () =>
        [validateEmail(), validateUsername()].every(Boolean);

    const handleClickRegister = async () => {
        const { isLoading, email, password, username } = state;
        const valid = validateForm();
        if (!valid || isLoading) {
            return;
        }
        setState({ ...state, isLoading: true });
        try {
            await this.props.registration({ email, password, username });
        } catch {
            // TODO: show error
            setState({ ...state, isLoading: false });
        }
    };
    return (
        <Grid container textAlign="center" style={{ paddingTop: 30 }}>
            <Grid.Column>
                <Form
                    name="registrationForm"
                    size="large"
                    onSubmit={handleClickRegister}
                >
                    <Image
                        centered
                        src={getUserImgLink(user.image)}
                        size="medium"
                        circular
                    />
                    <br />
                    <Input
                        icon="user"
                        iconPosition="left"
                        placeholder="Username"
                        type="text"
                        error={!state.isUsernameValid}
                        onChange={ev => usernameChanged(ev.target.value)}
                        value={state.username}
                        onBlur={validateUsername}
                    />
                    <br />
                    <br />
                    <Input
                        icon="at"
                        iconPosition="left"
                        placeholder="Email"
                        type="email"
                        error={!state.isEmailValid}
                        value={state.email}
                        onChange={ev => emailChanged(ev.target.value)}
                        onBlur={validateEmail}
                    />
                    <Button
                        type="submit"
                        color="teal"
                        loading={state.isLoading}
                        size="large"
                        primary
                    >
                        Change
                    </Button>
                </Form>
            </Grid.Column>
        </Grid>
    );
};

Profile.propTypes = {
    user: PropTypes.objectOf(PropTypes.any)
};

Profile.defaultProps = {
    user: {}
};

const mapStateToProps = rootState => ({
    user: rootState.profile.user
});

export default connect(mapStateToProps)(Profile);
