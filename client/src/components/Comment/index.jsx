import React from "react";
import PropTypes from "prop-types";
import { Comment as CommentUI, Label, Icon, Button } from "semantic-ui-react";
import moment from "moment";
import { getUserImgLink } from "src/helpers/imageHelper";

import UpdatedModal from "../Modal";

import styles from "./styles.module.scss";

const Comment = props => {
    const {
        currentUserId,
        deleteComment,
        updateComment,
        restoreComment,
        comment: { id, body, createdAt, user, userId, postId, deletedAt }
    } = props;
    const date = moment(createdAt).fromNow();
    if (userId !== currentUserId && deletedAt) {
        return null;
    }
    const content =
        userId === currentUserId &&
        (deletedAt ? (
            <Button onClick={() => restoreComment(id, postId)}>Restore</Button>
        ) : (
            <React.Fragment>
                <UpdatedModal
                    update={updateComment}
                    body={body}
                    id={id}
                    postId={postId}
                />
                <Label
                    basic
                    size="small"
                    as="a"
                    className={styles.toolbarBtn}
                    onClick={() => deleteComment(postId, id)}
                >
                    <Icon name="trash" />
                </Label>
            </React.Fragment>
        ));
    return (
        <CommentUI className={styles.comment}>
            <CommentUI.Avatar src={getUserImgLink(user.image)} />
            <CommentUI.Content>
                <CommentUI.Author as="a">{user.username}</CommentUI.Author>
                <CommentUI.Metadata>
                    {date}
                    {content}
                </CommentUI.Metadata>
                <CommentUI.Text>{body}</CommentUI.Text>
            </CommentUI.Content>
        </CommentUI>
    );
};

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Comment;
