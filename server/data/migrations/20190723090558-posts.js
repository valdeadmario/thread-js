"use strict";

module.exports = {
    up: (queryInterface, Sequelize) =>
        queryInterface.sequelize.transaction(transaction =>
            Promise.all([
                queryInterface.addColumn(
                    "posts",
                    "deletedAt",
                    {
                        type: Sequelize.DATE
                    },
                    { transaction }
                ),
                queryInterface.addColumn(
                    "comments",
                    "deletedAt",
                    {
                        type: Sequelize.DATE
                    },
                    { transaction }
                )
            ])
        ),

    down: (queryInterface, Sequelize) =>
        queryInterface.sequelize.transaction(transaction =>
            Promise.all([
                queryInterface.removeColumn("posts", "deletedAt", {
                    transaction
                }),
                queryInterface.removeColumn("comments", "deletedAt", {
                    transaction
                })
            ])
        )
};
