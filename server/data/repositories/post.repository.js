import sequelize from "../db/connection";
import { Op } from "../db/connection";
import {
    PostModel,
    CommentModel,
    UserModel,
    ImageModel,
    PostReactionModel
} from "../models/index";
import BaseRepository from "./base.repository";

const likeCase = bool =>
    `CASE WHEN "postReactions"."isLike" = ${bool} THEN 1 ELSE 0 END`;

class PostRepository extends BaseRepository {
    async getPosts(filter) {
        const {
            from: offset,
            count: limit,
            userId,
            not,
            liked,
            deleted
        } = filter;
        let where = {};
        let paranoid = true;
        if (userId) {
            if (not === "true") {
                where = {
                    userId: {
                        [Op.ne]: userId
                    }
                };
            } else if (liked === "true") {
                const postReactions = await PostReactionModel.findAll({
                    where: { userId, isLike: true },
                    attributes: ["postId"]
                });
                where = {
                    id: { [Op.in]: postReactions.map(react => react.postId) }
                };
            } else if (deleted === "true") {
                where = {
                    userId,
                    deletedAt: {
                        [Op.ne]: null
                    }
                };
                paranoid = false;
            } else {
                where = { userId };
            }
        }

        return this.model.findAll({
            where,
            paranoid,
            attributes: {
                include: [
                    [
                        sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" is NULL )`),
                        "commentCount"
                    ],
                    [
                        sequelize.fn("SUM", sequelize.literal(likeCase(true))),
                        "likeCount"
                    ],
                    [
                        sequelize.fn("SUM", sequelize.literal(likeCase(false))),
                        "dislikeCount"
                    ]
                ]
            },
            include: [
                {
                    model: ImageModel,
                    attributes: ["id", "link"]
                },
                {
                    model: UserModel,
                    attributes: ["id", "username"],
                    include: {
                        model: ImageModel,
                        attributes: ["id", "link"]
                    }
                },
                {
                    model: PostReactionModel,
                    attributes: [],
                    duplicating: false
                }
            ],
            group: ["post.id", "image.id", "user.id", "user->image.id"],
            order: [["createdAt", "DESC"]],
            offset,
            limit
        });
    }

    getPostById(id) {
        return this.model.findOne({
            group: [
                "post.id",
                "comments.id",
                "comments->user.id",
                "comments->user->image.id",
                "user.id",
                "user->image.id",
                "image.id"
            ],
            where: { id },
            attributes: {
                include: [
                    [
                        sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`),
                        "commentCount"
                    ],
                    [
                        sequelize.fn("SUM", sequelize.literal(likeCase(true))),
                        "likeCount"
                    ],
                    [
                        sequelize.fn("SUM", sequelize.literal(likeCase(false))),
                        "dislikeCount"
                    ]
                ]
            },
            include: [
                {
                    model: CommentModel,
                    paranoid: false,
                    include: {
                        model: UserModel,
                        attributes: ["id", "username"],
                        include: {
                            model: ImageModel,
                            attributes: ["id", "link"]
                        }
                    }
                },
                {
                    model: UserModel,
                    attributes: ["id", "username"],
                    include: {
                        model: ImageModel,
                        attributes: ["id", "link"]
                    }
                },
                {
                    model: ImageModel,
                    attributes: ["id", "link"]
                },
                {
                    model: PostReactionModel,
                    attributes: []
                }
            ]
        });
    }
}

export default new PostRepository(PostModel);
