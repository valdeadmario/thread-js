import { Router } from "express";
import * as sendService from "../services/send.service";

const router = Router();

router.post("/", (req, res, next) => {
    sendService.sendLink(req.body.link, req.body.email);
    req.io.emit("email_request");
});

export default router;
