import { Router } from "express";
import * as commentService from "../services/comment.service";

const router = Router();

router
    .get("/:id", (req, res, next) =>
        commentService
            .getCommentById(req.params.id)
            .then(comment => res.send(comment))
            .catch(next)
    )
    .post("/", (req, res, next) =>
        commentService
            .create(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
            .then(comment => res.send(comment))
            .catch(next)
    )
    .delete("/:id", (req, res, next) =>
        commentService
            .deleteComment(req.params.id)
            .then(() => res.send({ deleted: "true" }))
            .catch(next)
    )
    .put("/:id", (req, res, next) =>
        !req.body.restore
            ? commentService
                  .updateComment(req.params.id, req.body)
                  .then(post => res.send(post))
                  .catch(next)
            : commentService
                  .restoreComment(req.params.id)
                  .then(post => res.send(post))
                  .catch(next)
    );

export default router;
