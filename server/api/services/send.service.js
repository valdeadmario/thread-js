const nodemailer = require("nodemailer");

export const sendLink = (link, email) => {
    const output = `
    <p>You have a new contact request</p>
    <h2>Hello dear ${email}</h2>
    <p>Someone shared a post with you </p>
    <p>${link}</p>
    <h3>Join us</h3>
  `;

    let transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
            user: "thread5228@gmail.com",
            pass: "qwer123ty"
        }
    });

    const mailOptions = {
        from: "thread5228@gmail.com",
        to: email,
        subject: "Subject of your email",
        html: output
    };

    transporter.sendMail(mailOptions, function(err, info) {
        if (err) console.log(err);
        else console.log(info);
    });
};
