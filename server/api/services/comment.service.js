import commentRepository from "../../data/repositories/comment.repository";

export const create = (userId, comment) =>
    commentRepository.create({
        ...comment,
        userId
    });

export const getCommentById = id => commentRepository.getCommentById(id);

export const deleteComment = async id => commentRepository.deleteById(id);

export const updateComment = async (id, data) =>
    await commentRepository.updateById(id, data);

export const restoreComment = id => commentRepository.restoreById(id);
